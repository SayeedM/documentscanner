﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace TigerIT.ImageProcessing.DocumentScanner
{
    public class ScannerOption
    {
        private bool _doCleanup = true;
        public bool DoCleanupAfterScan { 
            get { return _doCleanup; }
            set { _doCleanup = value; } 
        }

        private ScanType _scanType = ScanType.ConvertToGrayscale;
        public ScanType ScanType {
            get { return _scanType; }
            set { _scanType = value; }
        }

        private GrayscaleTextWeight _textWeight = GrayscaleTextWeight.NONE;
        public GrayscaleTextWeight TextWeight{
            get { return _textWeight; }
            set { _textWeight = value; }
        }

        private bool _cropTextContents = false;
        public bool CropTextContets{
            get { return _cropTextContents; }
            set { _cropTextContents = value; }
        }

        private bool _idCardMode = false;
        public bool IdCardMode{
            get { return _idCardMode; }
            set { _idCardMode = value; }
        }

        //public bool _markTextContents = false;
        //public bool MarkTextContets{
        //    get { return _markTextContents; }
        //    set { _markTextContents = value; }
        //}

        public static ScannerOption createDefaultOptions(){
            ScannerOption option = new ScannerOption();
            
            //not necessarry at all
            option.DoCleanupAfterScan = true;
            option.ScanType = ScanType.ConvertToGrayscale;
            option.TextWeight = GrayscaleTextWeight.NORMAL;
            option.CropTextContets = true;
            option.IdCardMode = false;

            return option;
        }

        

        
    }
}
