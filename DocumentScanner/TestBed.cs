﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Emgu.CV;
using Emgu.CV.Structure;
using System.Drawing;
using System.Diagnostics;
using Emgu.CV.CvEnum;

namespace TigerIT.ImageProcessing.DocumentScanner
{
    public class TestBed
    {
        public static Bitmap detectPaperTest(Bitmap image){

            Image<Bgr, Byte> inputImage = new Image<Bgr, Byte>(image);
            Image<Gray, Byte> temp = new Image<Gray, Byte>(image);
            //Rectangle roi = new Rectangle(15, 15, image.Width - 15, image.Height - 15);
            //inputImage.ROI = roi;
            //inputImage = inputImage.Copy();


            /**
             * Heuristic - The paper must occupy half of the image in both width and height
             */ 
            double threshHeight = inputImage.Size.Height * 0.1;
            double threshWidth = inputImage.Size.Width * 0.1;

            //grayscale conversion
            Image<Gray, Byte> grayImage = inputImage.Convert<Gray, Byte>();

            IntPtr morphKernel = CvInvoke.cvCreateStructuringElementEx(3, 3, 0, 0, CV_ELEMENT_SHAPE.CV_SHAPE_CROSS, IntPtr.Zero);
            CvInvoke.cvMorphologyEx(grayImage, grayImage, temp, morphKernel, CV_MORPH_OP.CV_MOP_GRADIENT, 10);
            //CvInvoke.cvThreshold(grayImage, grayImage, 240, 255, THRESH.CV_THRESH_BINARY | THRESH.CV_THRESH_OTSU);

            grayImage = grayImage.Erode(5);
            
            //return grayImage.ToBitmap();

            //return grayImage.ToBitmap();
            
            //blurring
            Image<Gray, Byte> blurredImage = new Image<Gray, Byte>(grayImage.Size);
            blurredImage = grayImage.SmoothMedian(9);

            grayImage = grayImage.Erode(5);
            grayImage = grayImage.Dilate(5);

            //return blurredImage.ToBitmap();
    
            //canny edge conversion
            Image<Gray, Byte> cannyImage = new Image<Gray, Byte>(blurredImage.Size);
            CvInvoke.cvCanny(blurredImage, cannyImage, 75, 200, 5);

            cannyImage = cannyImage.Dilate(1);
            //cannyImage = cannyImage.Erode(5);

            //return cannyImage.ToBitmap();

            //return cannyImage.ToBitmap();
            /* finding contours that resemble to a quadrangle */
            MemStorage storage = new MemStorage();
            List<Contour<Point>> candidateList = new List<Contour<Point>>();
            List<double> areaList = new List<double>();

            for (Contour<Point> contours = cannyImage.FindContours(Emgu.CV.CvEnum.CHAIN_APPROX_METHOD.CV_CHAIN_APPROX_SIMPLE, Emgu.CV.CvEnum.RETR_TYPE.CV_RETR_LIST, storage); contours != null; contours = contours.HNext){
                Contour<Point> currentContour = contours.ApproxPoly(contours.Perimeter * 0.02, storage);
                if (currentContour.Count() >= 4){
                    candidateList.Add(currentContour);
                    areaList.Add(currentContour.Area);
                }
            }
            
            /* finding the biggest one */
            double area = -1.0;
            Contour<Point> paper = null;
            for (int i = 0 ; i < candidateList.Count ; i++){
                if (areaList[i] > area){
                    area = areaList[i];
                    paper = candidateList[i];
                    inputImage.Draw(candidateList[i], new Bgr(0, 0, 255), 4);
                }
            }

            return inputImage.ToBitmap();
            
            //if (paper != null){
            //    if (paper.BoundingRectangle.Width >= threshWidth && paper.BoundingRectangle.Height > threshHeight){
            //        return paper;
            //    }
            //}

            //return cannyImage.ToBitmap();

            inputImage.Dispose();
            grayImage.Dispose();
            blurredImage.Dispose();
            cannyImage.Dispose();

        
        }

        public static Bitmap detectPaperUsingChannelSeperation(Bitmap image){
            MemStorage storage = new MemStorage();
            List<Contour<Point>> candidateList = new List<Contour<Point>>();
            List<double> areaList = new List<double>();

            Image<Bgr, Byte> inputImage = new Image<Bgr, Byte>(image);
            //Rectangle roi = new Rectangle(15, 15, image.Width - 15, image.Height - 15);
            //inputImage.ROI = roi;
            //inputImage = inputImage.Copy();

            double threshHeight = inputImage.Size.Height * 0.50;
            double threshWidth = inputImage.Size.Width * 0.50;


            //std::vector<std::vector<cv::Point> > squares;
            //cv::Mat pyr, timg, gray0(_image.size(), CV_8U), gray;
            int thresh = 50, N = 5;
            //cv::pyrDown(_image, pyr, cv::Size(_image.cols/2, _image.rows/2));
            //cv::pyrUp(pyr, timg, _image.size());
            //std::vector<std::vector<cv::Point> > contours;

            Image<Gray, Byte> [] gray0 = new Image<Gray,byte>[3];
            
            
            for( int c = 0; c < 3; c++ ) {
                try{
                    int [] ch = {c, 0};
                    gray0[c] = new Image<Gray,byte>(inputImage.Size);
                    CvInvoke.cvMixChannels(new IntPtr[] { inputImage }, 1, new IntPtr[]{gray0[c]}, 1, ch, 1);
                    Image<Gray, Byte> gray = new Image<Gray,byte>(inputImage.Size);
                    for (int l = 0 ; l < N ; l++){
                        if (l == 0){
                            Image<Gray, Byte> cannyImage = gray0[c].Canny(0, thresh, 5);
                            CvInvoke.cvDilate(cannyImage, gray, IntPtr.Zero, 1);
                            //CvInvoke.cvShowImage("ch " + c + "-" + l, gray);
                        }else{
                            CvInvoke.cvThreshold(gray0[c], gray, (l + 1)*255/N, 255, Emgu.CV.CvEnum.THRESH.CV_THRESH_BINARY);
                            //CvInvoke.cvShowImage("ch " + c + "-" + l, gray0[c]);
                        }

                        //CvInvoke.cvShowImage("image", gray);    

                        for (Contour<Point> contours = gray.FindContours(Emgu.CV.CvEnum.CHAIN_APPROX_METHOD.CV_CHAIN_APPROX_SIMPLE, Emgu.CV.CvEnum.RETR_TYPE.CV_RETR_LIST, storage); contours != null; contours = contours.HNext){
                            Contour<Point> currentContour = contours.ApproxPoly(contours.Perimeter * 0.02, storage);
                            if (currentContour.Count() >= 4){
                                if (currentContour.Area > 3000){
                                //if (currentContour.BoundingRectangle.Width >= threshWidth && currentContour.BoundingRectangle.Height > threshHeight){
                                    candidateList.Add(currentContour);
                                    areaList.Add(currentContour.Area);
                                    inputImage.Draw(currentContour, new Bgr(255, 0, 0), 1);
                                }
                            }
                        }
                    }
                    
                    
                    
                    
                }catch(Exception ex){
                    Debug.WriteLine(ex.Message);
                }

                
            }

            /* finding the biggest one */
            double area = -1.0;
            Contour<Point> paper = null;
            for (int i = 0 ; i < candidateList.Count ; i++){
                if (areaList[i] > area){
                    area = areaList[i];
                    paper = candidateList[i];
                }
            }
            
            if (paper != null){
                if (paper.BoundingRectangle.Width >= threshWidth && paper.BoundingRectangle.Height > threshHeight){
                    inputImage.Draw(paper, new Bgr(0, 0, 255), 2);
                }
            }
            return inputImage.ToBitmap();
                
                //for( int l = 0; l < N; l++ ) {
                //    if( l == 0 ) {
                //        cv::Canny(gray0, gray, 0, thresh, 5);
                //        cv::dilate(gray, gray, cv::Mat(), cv::Point(-1,-1));
                //    }
                //    else {
                //        gray = gray0 >= (l+1)*255/N;
                //    }
                //    cv::findContours(gray, contours, CV_RETR_LIST, CV_CHAIN_APPROX_SIMPLE);
                //    std::vector<cv::Point> approx;
                //    for( size_t i = 0; i < contours.size(); i++ )
                //    {
                //        cv::approxPolyDP(cv::Mat(contours[i]), approx, arcLength(cv::Mat(contours[i]), true)*0.02, true);
                //        if( approx.size() == 4 && fabs(contourArea(cv::Mat(approx))) > 1000 && cv::isContourConvex(cv::Mat(approx))) {
                //            double maxCosine = 0;

                //            for( int j = 2; j < 5; j++ )
                //            {
                //                double cosine = fabs(angle(approx[j%4], approx[j-2], approx[j-1]));
                //                maxCosine = MAX(maxCosine, cosine);
                //            }

                //            if( maxCosine < 0.3 ) {
                //                squares.push_back(approx);
                //            }
                //        }
                //    }
                //}
            }
        //}


        public static Bitmap detectCard(Bitmap bmp){

            Image<Bgr, Byte>  inputImage = new Image<Bgr,byte>(bmp);
            Rectangle roi = new Rectangle(30, 30, inputImage.Width - 30, inputImage.Height - 30);
            inputImage.ROI = roi;
            Image<Bgr, Byte> inputImageCrop = new Image<Bgr, Byte>(inputImage.Width - 60, inputImage.Height - 60);
            inputImageCrop = inputImage.Copy();

            Image<Bgr, Byte> inputImageClone = inputImageCrop.Clone(); 
            inputImageClone._EqualizeHist();

            Image<Gray, Byte> gray = inputImageClone.Convert<Gray, Byte>();
            

            
            
            gray = gray.Erode(1);
            gray = gray.SmoothMedian(11);
            

            CvInvoke.cvThreshold(gray, gray, 15, 255, THRESH.CV_THRESH_BINARY_INV);

            
            

            MemStorage storage = new MemStorage();
            List<Contour<Point>> candidateList = new List<Contour<Point>>();
            List<double> areaList = new List<double>();

            for (Contour<Point> contours = gray.FindContours(Emgu.CV.CvEnum.CHAIN_APPROX_METHOD.CV_CHAIN_APPROX_SIMPLE, Emgu.CV.CvEnum.RETR_TYPE.CV_RETR_LIST, storage); contours != null; contours = contours.HNext){
                Contour<Point> currentContour = contours.ApproxPoly(contours.Perimeter * 0.02, storage);
                if (currentContour.Count() >= 4){
                    
                    candidateList.Add(currentContour);
                    areaList.Add(currentContour.Area);
                }
            }
            
            /* finding the biggest one */
            double area = -1.0;
            Contour<Point> paper = null;
            for (int i = 0 ; i < candidateList.Count ; i++){
                if (areaList[i] > area){
                    area = areaList[i];
                    paper = candidateList[i];
                    //inputImageCrop.Draw(candidateList[i], new Bgr(0, 0, 255), 4);
                }
            }
            inputImageCrop.Draw(paper, new Bgr(0, 0, 255), 4);

            return inputImageCrop.ToBitmap();



        }


        private static Point computeIntersect(LineSegment2D a, LineSegment2D b) 
                             
        {

	        int x1 = a.P1.X, 
                y1 = a.P1.Y, 
                x2 = a.P2.X, 
                y2 = a.P2.Y, 
                x3 = b.P1.X, 
                y3 = b.P1.Y, 
                x4 = b.P2.X, 
                y4 = b.P2.Y;
	        
            float denom;
            float d = ((float)(x1 - x2) * (y3 - y4)) - ((y1 - y2) * (x3 - x4));
	        if (d > 0)
	        {
		        Point pt = new Point();
		        pt.X = (int)(((x1 * y2 - y1 * x2) * (x3 - x4) - (x1 - x2) * (x3 * y4 - y3 * x4)) / d);
		        pt.Y = (int)(((x1 * y2 - y1 * x2) * (y3 - y4) - (y1 - y2) * (x3 * y4 - y3 * x4)) / d);
		        return pt;
	        }
	        else
		        return new Point(-1, -1);
        }

    

    private Image<Gray, Byte> postProcessAdaptive(Image<Gray, Byte> input){
            Image<Gray, Byte> output = new Image<Gray,Byte>(input.Size); 
            CvInvoke.cvAdaptiveThreshold(input, output, 250, ADAPTIVE_THRESHOLD_TYPE.CV_ADAPTIVE_THRESH_GAUSSIAN_C, THRESH.CV_THRESH_BINARY, 19, 24);
            return output;
        }

        private Image<Gray, Byte> postProcessSmoothAdaptive(Image<Gray, Byte> input){
            Image<Gray, Byte> output = new Image<Gray,Byte>(input.Size);
            Image<Gray, Byte> blur = input.SmoothBlur(19, 19);
            Image<Gray, Byte> diff = blur.Clone();
            Image<Gray, Byte> threshold = blur.Clone();
            Image<Gray, Byte> sharpened = blur.Clone();

            CvInvoke.cvAbsDiff(input, blur, diff);
            CvInvoke.cvThreshold(blur, threshold, 48, 1, THRESH.CV_THRESH_BINARY);
            CvInvoke.cvMul(blur, threshold, blur, 1);
            CvInvoke.cvAddWeighted(input, 2, blur, -1, 0, sharpened);
            CvInvoke.cvAdaptiveThreshold(sharpened, output, 255, ADAPTIVE_THRESHOLD_TYPE.CV_ADAPTIVE_THRESH_GAUSSIAN_C, THRESH.CV_THRESH_BINARY, 19, 24);
            return output;
            
        }

        private Image<Gray, Byte> postProcessSmoothOtsu(Image<Gray, Byte> input){
            Image<Gray, Byte> output = new Image<Gray,Byte>(input.Size);
            Image<Gray, Byte> blur = input.SmoothBlur(19, 19);
            Image<Gray, Byte> diff = blur.Clone();
            Image<Gray, Byte> threshold = blur.Clone();
            Image<Gray, Byte> sharpened = blur.Clone();

            CvInvoke.cvAbsDiff(input, blur, diff);
            CvInvoke.cvThreshold(blur, threshold, 48, 1, THRESH.CV_THRESH_BINARY);
            CvInvoke.cvMul(blur, threshold, blur, 1);
            CvInvoke.cvAddWeighted(input, 2, blur, -1, 0, sharpened);
            CvInvoke.cvThreshold(sharpened, output, 0, 255, THRESH.CV_THRESH_OTSU);
            output = output.SmoothMedian(3);
            return output;
            
        }

        private Image<Gray, Byte> postProcessTest(Image<Gray, Byte> input){
            Image<Gray, Byte> output = input.SmoothGaussian(0, 0, 3, 0);
            return output;
        }

        private Image<Bgr, float> postProcessDenoise(Image<Bgr, Byte> input){
            float[,] k = { {0, 0, 0},
                           {0, 0, -0},
                           {0.33F, 0, -0}};

            ConvolutionKernelF kernel = new ConvolutionKernelF(k);

            Image<Bgr, float> convolutedImage = input * kernel;

            return convolutedImage;
        }

        private Image<Gray, Byte> postProcessTest2(Image<Gray, Byte> input){
            Image<Gray, Byte> output = new Image<Gray,Byte>(input.Size);
            CvInvoke.cvThreshold(input, output, 162, 255, THRESH.CV_THRESH_BINARY);
            output = output.SmoothGaussian(3, 3, 0, 0);
            return output;
        }

        private Image<Gray, Byte> postProcessTest3(Image<Gray, Byte> input){
            Image<Gray, Byte> output = new Image<Gray,Byte>(input.Size);
            output = input.SmoothGaussian(3, 3, 0, 0);
            CvInvoke.cvAdaptiveThreshold(output, output, 255, ADAPTIVE_THRESHOLD_TYPE.CV_ADAPTIVE_THRESH_GAUSSIAN_C, THRESH.CV_THRESH_BINARY, 75, 10);
            return output;
        }

        private Image<Gray, Byte> postProcessTest4(Image<Gray, Byte> input){
            Image<Gray, Byte> output = new Image<Gray,Byte>(input.Size);
            output = input.Clone(); //input.SmoothMedian(5);
            CvInvoke.cvThreshold(output, output, 255, 255, THRESH.CV_THRESH_TOZERO | THRESH.CV_THRESH_OTSU);
            return output;
        }

        private Image<Gray, Byte> postProcessTest5(Image<Gray, Byte> input){
            Image<Gray, Byte> output = new Image<Gray,Byte>(input.Size);
            output = input.Clone(); //input.SmoothMedian(5);
            CvInvoke.cvThreshold(output, output, 255, 255, THRESH.CV_THRESH_TOZERO | THRESH.CV_THRESH_OTSU);

            Image<Gray, Byte> output2 = output.SmoothGaussian(0, 0, 3, 0);
            CvInvoke.cvAddWeighted(output, 2.5, output2, -0.5, 0, output);

            Image<Gray, Byte> output3 = output.SmoothGaussian(0, 0, 3, 0);
            CvInvoke.cvAddWeighted(output, 2.5, output3, -0.5, 0, output);

            output = output.SmoothBilatral(9, 75, 75);

            return output;
        }

        


        

       
       

        

    }

    
}
