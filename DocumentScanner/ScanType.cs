﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TigerIT.ImageProcessing.DocumentScanner
{
    public enum ScanType
    {
        ConvertToGrayscale, AsIs
    }
}
