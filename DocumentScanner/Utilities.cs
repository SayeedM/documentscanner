﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace TigerIT.ImageProcessing.DocumentScanner
{
    public class Utilities
    {
        //Sorts a vector of 4 points into top left, top right, bottomleft, bottomright
        public static Point [] sortFourPoints(Point []  unsorted) {
            Point [] sorted = new Point[4];
            sorted[0] = new Point(0, 0);
            sorted[1] = new Point(0, 0);
            sorted[2] = new Point(0, 0);
            sorted[3] = new Point(0, 0);

            
            int middleX = (unsorted[0].X + unsorted[1].X + unsorted[2].X + unsorted[3].X) / 4;
            int middleY = (unsorted[0].Y + unsorted[1].Y + unsorted[2].Y + unsorted[3].Y) / 4;
            
            for (int i = 0; i < unsorted.Length; i++) {
                if (unsorted[i].X < middleX && unsorted[i].Y < middleY)
                    sorted[0] = unsorted[i];
                if (unsorted[i].X > middleX && unsorted[i].Y < middleY)
                    sorted[1] = unsorted[i];
                if (unsorted[i].X < middleX && unsorted[i].Y > middleY)
                    sorted[2] = unsorted[i];
                if (unsorted[i].X > middleX && unsorted[i].Y > middleY)
                    sorted[3] = unsorted[i];
            }
            return sorted;
        }
    }


}
