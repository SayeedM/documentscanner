﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TigerIT.ImageProcessing.DocumentScanner
{
    public enum GrayscaleTextWeight
    {
        NONE = 0, 
        NORMAL = 1,
        HEAVY = 2
    }
}
