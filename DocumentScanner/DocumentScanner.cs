﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using Emgu.CV.Structure;
using Emgu.CV;
using Emgu.CV.CvEnum;
using System.Diagnostics;

namespace TigerIT.ImageProcessing.DocumentScanner
{
    public class DocumentScanner
    {
        private ScannerOption option ;
        
        public DocumentScanner(ScannerOption option){
            this.option = option;
        }


        
        private Image<Bgr, Byte> inputImage = null ;
        private Image<Gray, Byte> grayImage = null ;
        private Image<Gray, Byte> blurredImage = null ;
        private Image <Gray, Byte> cannyImage = null ;
        private Image<Bgr, Byte> sample = null;
        private Image<Gray, Byte> graySample = null;
        private Image<Gray, Byte> temp = null ;
        private Image<Bgr, Byte> tempColor = null ;
        Image<Bgr, Byte> inputImageCrop = null ;
        Image<Bgr, Byte> inputImageClone = null;
        private MemStorage storage = null;
        

        private double threshHeight = 0 ;
        private double threshWidth = 0 ;

        private int CANNY_THRESH_PAPER = 3;
        private int CANNY_THRESH_CARD = 5;

        

        public Bitmap Scan(Bitmap image){
            resetBuffers();

            inputImage = new Image<Bgr, Byte>(image);
            


            /**
             * Heuristic - The paper must occupy half of the image in both width and height
             */
            if (!option.IdCardMode){
                threshHeight = inputImage.Size.Height * 0.50;
                threshWidth = inputImage.Size.Width * 0.50;
            }else{
                threshHeight = (inputImage.Size.Height - 60) * 0.10;
                threshWidth = (inputImage.Size.Width - 60) * 0.10;
            }

            if (option.IdCardMode){
                Rectangle roi = new Rectangle(30, 30, inputImage.Width - 30, inputImage.Height - 30);
                inputImage.ROI = roi;
                inputImageCrop = new Image<Bgr, Byte>(inputImage.Width - 60, inputImage.Height - 60);
                inputImageCrop = inputImage.Copy();

                inputImageClone = inputImageCrop.Clone(); 
                inputImageClone._EqualizeHist();
                grayImage = inputImageClone.Convert<Gray, Byte>();
            }else{
               //grayscale conversion
                grayImage = inputImage.Convert<Gray, Byte>();
            }
            
    
            //canny edge conversion
            cannyImage = new Image<Gray, Byte>(grayImage.Size);


            if (option.IdCardMode){
                grayImage = grayImage.Erode(1);
                grayImage = grayImage.SmoothMedian(11);
                CvInvoke.cvThreshold(grayImage, cannyImage, 15, 255, THRESH.CV_THRESH_BINARY_INV);

                //blurredImage = grayImage.SmoothBlur(3, 3);
                ////CvInvoke.cvCanny(blurredImage, cannyImage, 75, 200, CANNY_THRESH_CARD);
                //CvInvoke.cvCanny(blurredImage, cannyImage, 100, 100, 5);
                //cannyImage = cannyImage.Dilate(5);
                //cannyImage = cannyImage.Erode(2);
            }else{
                //blurring
                blurredImage = new Image<Gray, Byte>(grayImage.Size);
                blurredImage = grayImage.SmoothMedian(9);
                CvInvoke.cvCanny(blurredImage, cannyImage, 75, 200, CANNY_THRESH_PAPER);
            }

            Contour<Point> paper = findPaper(cannyImage);

            
            if (paper != null){
                /* paper found -- need to crop */
                sample = cropAndDeskew(paper, inputImage);
            }else{
                /**
                 * assume the paper covers the total image
                 * Here is one catch though -- there might be no paper at all
                 * well in that case we just return the total image - do whatever you like to do with your
                 * original image
                 */ 
                sample = inputImage;
            }

            if (option.ScanType == ScanType.ConvertToGrayscale){
                graySample = sample.Convert<Gray, Byte>();

                if (option.DoCleanupAfterScan)
                    graySample = cleanupGrayscale(graySample);

                graySample = graySample.Erode((int)option.TextWeight);

                if (!option.IdCardMode){
                    if (option.CropTextContets){
                        MCvBox2D box = getBoundingBoxForContents(graySample.Clone());
                        //graySample.Draw(box, new Gray(0), 2);
                        graySample = cropAndDeskew(box, graySample);
                    }
                }

                return graySample.ToBitmap();

            }else{
                if (option.DoCleanupAfterScan)
                    sample = cleanupRgb(sample);

                sample = sample.Erode((int)option.TextWeight);

                if (option.CropTextContets){
                    MCvBox2D box = getBoundingBoxForContents(sample.Convert<Gray, Byte>());
                    sample = cropAndDeskew(box, sample);
                }
                return sample.ToBitmap();
            } 
        }

        /**
         * Find the contour that represents paper which also satisfies our heuristics
         * Returns null if not found
         */ 
        private Contour<Point> findPaper(Image<Gray, Byte> cannyImage){
            /* finding contours that resemble to a quadrangle */
            List<Contour<Point>> candidateList = new List<Contour<Point>>();
            List<double> areaList = new List<double>();

            for (Contour<Point> contours = cannyImage.FindContours(Emgu.CV.CvEnum.CHAIN_APPROX_METHOD.CV_CHAIN_APPROX_SIMPLE, Emgu.CV.CvEnum.RETR_TYPE.CV_RETR_LIST, storage); contours != null; contours = contours.HNext){
                Contour<Point> currentContour = contours.ApproxPoly(contours.Perimeter * 0.02, storage);
                if (currentContour.Count() >= 4){
                    candidateList.Add(currentContour);
                    areaList.Add(currentContour.Area);
                }
            }
            
            /* finding the biggest one */
            double area = -1.0;
            Contour<Point> paper = null;
            for (int i = 0 ; i < candidateList.Count ; i++){
                if (areaList[i] > area){
                    area = areaList[i];
                    paper = candidateList[i];
                }
            }
            
            if (paper != null){
                if (option.IdCardMode)
                    return paper;

                if (paper.BoundingRectangle.Width >= threshWidth && paper.BoundingRectangle.Height >= threshHeight){
                    return paper;
                }
            }
            return null;
        }

        /**
         * Crop the paper and straighten it
         */ 
        private Image<Bgr, Byte> cropAndDeskew(Contour<Point> paper, Image<Bgr, Byte> originalImage){
            
            Point [] sortedPts = Utilities.sortFourPoints(paper.ToArray<Point>());

            Point tl = sortedPts[0];
            Point tr = sortedPts[1];
            Point bl = sortedPts[2];
            Point br = sortedPts[3];

            //maximum width of the image
            double widthA = Math.Sqrt(((br.X - bl.X) * (br.X - bl.X)) + ((br.Y - bl.Y) * (br.Y - bl.Y)));
            double widthB = Math.Sqrt(((tr.X - tl.X) *(tr.X - tl.X)) + ((tr.Y - tl.Y) * (tr.Y - tl.Y)));
            int maxWidth = Math.Max((int)widthA, (int)widthB);

            //max height of the image
	        double heightA = Math.Sqrt(((tr.X - br.X) *(tr.X - br.X)) + ((tr.Y - br.Y) * (tr.Y - br.Y)));
	        double heightB = Math.Sqrt(((tl.X - bl.X) * (tl.X - bl.X)) + ((tl.Y - bl.Y) * (tl.Y - bl.Y)));
	        int maxHeight = Math.Max((int)heightA, (int)heightB);

            PointF [] srcPoint = new PointF[4];
            srcPoint[0] = new PointF(tl.X, tl.Y);
            srcPoint[1] = new PointF(tr.X, tr.Y);
            srcPoint[2] = new PointF(br.X, br.Y);
            srcPoint[3] = new PointF(bl.X, bl.Y);

            PointF [] dstPoint = new PointF[4];
            dstPoint[0] = new PointF(0, 0);
            dstPoint[1] = new PointF(maxWidth - 1, 0);
            dstPoint[2] = new PointF(maxWidth - 1, maxHeight - 1);
            dstPoint[3] = new PointF(0, maxHeight - 1);

                
                
            tempColor = new Image<Bgr,Byte>(maxWidth - 1, maxHeight - 1, new Bgr(255, 255, 255));
                
            IntPtr mapMatrix = CvInvoke.cvCreateMat(3, 3, MAT_DEPTH.CV_32F);
                                CvInvoke.cvGetPerspectiveTransform(srcPoint, dstPoint, mapMatrix);
            
            CvInvoke.cvWarpPerspective(originalImage, tempColor, mapMatrix, (int)INTER.CV_INTER_LINEAR, new MCvScalar(255, 255, 255));

            return tempColor;
            
        }

        /**
         * Crop the paper and straighten it
         */ 
        private Image<Gray, Byte> cropAndDeskew(MCvBox2D box, Image<Gray, Byte> originalImage){
            Point [] points = new Point[box.GetVertices().Length];
            PointF[] pointFs = box.GetVertices();
            for (int i = 0 ; i < box.GetVertices().Length ; i++){
                points[i] = new Point((int)pointFs[i].X, (int)pointFs[i].Y);
            }
            
            Point [] sortedPts = Utilities.sortFourPoints(points);

            Point tl = sortedPts[0];
            Point tr = sortedPts[1];
            Point bl = sortedPts[2];
            Point br = sortedPts[3];

            //maximum width of the image
            double widthA = Math.Sqrt(((br.X - bl.X) * (br.X - bl.X)) + ((br.Y - bl.Y) * (br.Y - bl.Y)));
            double widthB = Math.Sqrt(((tr.X - tl.X) *(tr.X - tl.X)) + ((tr.Y - tl.Y) * (tr.Y - tl.Y)));
            int maxWidth = Math.Max((int)widthA, (int)widthB);

            //max height of the image
	        double heightA = Math.Sqrt(((tr.X - br.X) *(tr.X - br.X)) + ((tr.Y - br.Y) * (tr.Y - br.Y)));
	        double heightB = Math.Sqrt(((tl.X - bl.X) * (tl.X - bl.X)) + ((tl.Y - bl.Y) * (tl.Y - bl.Y)));
	        int maxHeight = Math.Max((int)heightA, (int)heightB);

            PointF [] srcPoint = new PointF[4];
            srcPoint[0] = new PointF(tl.X, tl.Y);
            srcPoint[1] = new PointF(tr.X, tr.Y);
            srcPoint[2] = new PointF(br.X, br.Y);
            srcPoint[3] = new PointF(bl.X, bl.Y);

            PointF [] dstPoint = new PointF[4];
            dstPoint[0] = new PointF(0, 0);
            dstPoint[1] = new PointF(maxWidth - 1, 0);
            dstPoint[2] = new PointF(maxWidth - 1, maxHeight - 1);
            dstPoint[3] = new PointF(0, maxHeight - 1);

                
            //TODO: find out a way to make sure this is disposed    
            temp = new Image<Gray,Byte>(maxWidth - 1, maxHeight - 1, new Gray(255));
                
            IntPtr mapMatrix = CvInvoke.cvCreateMat(3, 3, MAT_DEPTH.CV_32F);
                                CvInvoke.cvGetPerspectiveTransform(srcPoint, dstPoint, mapMatrix);
            
            CvInvoke.cvWarpPerspective(originalImage, temp, mapMatrix, (int)INTER.CV_INTER_LINEAR, new MCvScalar(255, 255, 255));

            return temp;
            
        }

        /**
         * Crop the paper and straighten it
         */ 
        private Image<Bgr, Byte> cropAndDeskew(MCvBox2D box, Image<Bgr, Byte> originalImage){
            Point [] points = new Point[box.GetVertices().Length];
            PointF[] pointFs = box.GetVertices();
            for (int i = 0 ; i < box.GetVertices().Length ; i++){
                points[i] = new Point((int)pointFs[i].X, (int)pointFs[i].Y);
            }
            
            Point [] sortedPts = Utilities.sortFourPoints(points);

            Point tl = sortedPts[0];
            Point tr = sortedPts[1];
            Point bl = sortedPts[2];
            Point br = sortedPts[3];

            //maximum width of the image
            double widthA = Math.Sqrt(((br.X - bl.X) * (br.X - bl.X)) + ((br.Y - bl.Y) * (br.Y - bl.Y)));
            double widthB = Math.Sqrt(((tr.X - tl.X) *(tr.X - tl.X)) + ((tr.Y - tl.Y) * (tr.Y - tl.Y)));
            int maxWidth = Math.Max((int)widthA, (int)widthB);

            //max height of the image
	        double heightA = Math.Sqrt(((tr.X - br.X) *(tr.X - br.X)) + ((tr.Y - br.Y) * (tr.Y - br.Y)));
	        double heightB = Math.Sqrt(((tl.X - bl.X) * (tl.X - bl.X)) + ((tl.Y - bl.Y) * (tl.Y - bl.Y)));
	        int maxHeight = Math.Max((int)heightA, (int)heightB);

            PointF [] srcPoint = new PointF[4];
            srcPoint[0] = new PointF(tl.X, tl.Y);
            srcPoint[1] = new PointF(tr.X, tr.Y);
            srcPoint[2] = new PointF(br.X, br.Y);
            srcPoint[3] = new PointF(bl.X, bl.Y);

            PointF [] dstPoint = new PointF[4];
            dstPoint[0] = new PointF(10, 10);
            dstPoint[1] = new PointF(maxWidth - 1 + 30, 0);
            dstPoint[2] = new PointF(maxWidth - 1 + 30, maxHeight - 1 + 30);
            dstPoint[3] = new PointF(10, maxHeight - 1 + 30);

                
            
            tempColor = new Image<Bgr,Byte>(maxWidth - 1, maxHeight - 1, new Bgr(255, 255, 255));
                
            IntPtr mapMatrix = CvInvoke.cvCreateMat(3, 3, MAT_DEPTH.CV_32F);
                                CvInvoke.cvGetPerspectiveTransform(srcPoint, dstPoint, mapMatrix);
            
            CvInvoke.cvWarpPerspective(originalImage, tempColor, mapMatrix, (int)INTER.CV_INTER_LINEAR, new MCvScalar(255, 255, 255));

            return tempColor;
            
        }

        /**
         * Cleanup grayscale image
         */
        private Image<Gray, Byte> cleanupGrayscale(Image<Gray, Byte> input){
            Image<Gray, Byte> output = new Image<Gray,Byte>(input.Size);
            output = input.Clone(); 
            CvInvoke.cvThreshold(output, output, 255, 255, THRESH.CV_THRESH_TRUNC);

            Image<Gray, Byte> output2 = output.SmoothGaussian(21, 21, 21, 0);
            CvInvoke.cvAddWeighted(output, 2.2, output2, -0.75, 0, output); //--> this works for now
            return output;
        }

        /**
         * Cleanup and sharpen rgb image
         */
        private Image<Bgr, Byte> cleanupRgb(Image<Bgr, Byte> input){
            Image<Bgr, Byte> blurred = input.SmoothGaussian(5);
            CvInvoke.cvAddWeighted(input, 2.2, blurred, -.75, 0, input);
            return input;
        }

       
        

        /**
         * Get the area that represents contents of the page
         */ 
        private MCvBox2D getBoundingBoxForContents(Image<Gray, Byte> gray){
            
            temp = new Image<Gray,byte>(gray.Size);
            Image<Gray, Byte> grad = new Image<Gray,byte>(gray.Size);

            IntPtr morphKernel = CvInvoke.cvCreateStructuringElementEx(3, 3, 0, 0, CV_ELEMENT_SHAPE.CV_SHAPE_ELLIPSE, IntPtr.Zero);
            CvInvoke.cvMorphologyEx(gray, grad, temp, morphKernel, CV_MORPH_OP.CV_MOP_GRADIENT, 1);
            CvInvoke.cvThreshold(grad, grad, 0, 255, THRESH.CV_THRESH_BINARY | THRESH.CV_THRESH_OTSU);

            // connect horizontally oriented regions
            morphKernel = CvInvoke.cvCreateStructuringElementEx(9, 1, 0, 0, CV_ELEMENT_SHAPE.CV_SHAPE_RECT, IntPtr.Zero); //9, 1
            CvInvoke.cvMorphologyEx(grad, grad, temp, morphKernel, CV_MORPH_OP.CV_MOP_CLOSE, 1);
            
            grad = grad.Dilate(5);
            
            Image<Gray, Byte> mask = new Image<Gray,byte>(gray.Width, gray.Height, new Gray(0));
            Image<Gray, byte> maskRoi = null;
            
            //// find contours
            List<PointF> pts = new List<PointF>();
            storage = new MemStorage();
            
            for (var contour = grad.FindContours(
                CHAIN_APPROX_METHOD.CV_CHAIN_APPROX_SIMPLE, 
                RETR_TYPE.CV_RETR_CCOMP, 
                storage); contour!= null; contour = contour.HNext){
                    
                Rectangle rect = contour.BoundingRectangle;
                
                mask.Draw(contour, new Gray(255), -1);

                mask.ROI = rect;
                maskRoi = new Image<Gray, byte>(rect.Size);
                maskRoi = mask.Copy();
                CvInvoke.cvResetImageROI(mask);
                    
                double r = (double)maskRoi.CountNonzero()[0]/(rect.Width*rect.Height);
                /* assume at least 45% of the area is filled if it contains text */
                /* constraints on region size */
                /* these two conditions alone are not very robust. better to use something 
                like the number of significant peaks in a horizontal projection as a third condition */
                if (r > .2 && (rect.Height > 8 && rect.Width > 8)){
                    pts.AddRange(contour.GetMinAreaRect().GetVertices());
                }

            }
            

            grad.Dispose();
            mask.Dispose();
            
            if (maskRoi != null)
                maskRoi.Dispose();

            return PointCollection.MinAreaRect(pts.ToArray());
        }



        /**
         * Prepares the buffers
         */
        private void resetBuffers(){

            if (inputImage != null)
                inputImage.Dispose();

            if (grayImage != null)
                grayImage.Dispose();

            if (blurredImage != null)
                blurredImage.Dispose();

            if (cannyImage != null)
                cannyImage.Dispose();

            if (sample != null)
                sample.Dispose();

            if (graySample != null)
                graySample.Dispose();

            if (temp != null)
                temp.Dispose();

            if (tempColor != null)
                tempColor.Dispose();

            if (inputImageCrop != null)
                inputImageCrop.Dispose();

            if (inputImageClone != null)
                inputImageClone.Dispose();

            if (storage != null){
                storage.Clear();
                storage.Dispose();
            }

            storage = new MemStorage();

        }
    }
}
