﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;

namespace TigerIT.ImageProcessing.DocumentScanner
{
    public class CompressionUtility
    {
        public static bool CompressAndSave(Bitmap bmp, string path, long quality){
            EncoderParameter qualityParam = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, quality);

            // Jpeg image codec
            ImageCodecInfo jpegCodec = getEncoderInfo("image/jpeg");

            if (jpegCodec == null)
                return false;

            EncoderParameters encoderParams = new EncoderParameters(1);
            encoderParams.Param[0] = qualityParam;

            try{
                bmp.Save(path, jpegCodec, encoderParams);
            }catch(Exception ex){
                return false;
            }

            return true;
        }

        private static ImageCodecInfo getEncoderInfo(string mimeType)
        {
            // Get image codecs for all image formats
            ImageCodecInfo[] codecs = ImageCodecInfo.GetImageEncoders();

            // Find the correct image codec
            for (int i = 0; i < codecs.Length; i++)
            if (codecs[i].MimeType == mimeType)
            return codecs[i];
            return null;
        }
    }
}
