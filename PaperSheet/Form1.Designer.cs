﻿namespace PaperSheet
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.picPreview = new System.Windows.Forms.PictureBox();
            this.picCrop = new System.Windows.Forms.PictureBox();
            this.optGrayscale = new System.Windows.Forms.RadioButton();
            this.optAsIs = new System.Windows.Forms.RadioButton();
            this.chkCleanup = new System.Windows.Forms.CheckBox();
            this.chkCropContents = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.optWeightHeavy = new System.Windows.Forms.RadioButton();
            this.optWeightNormal = new System.Windows.Forms.RadioButton();
            this.optWeightNone = new System.Windows.Forms.RadioButton();
            this.lblWarning = new System.Windows.Forms.Label();
            this.chkIdCard = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.picPreview)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCrop)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 670);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "Load Image";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // picPreview
            // 
            this.picPreview.Location = new System.Drawing.Point(12, 12);
            this.picPreview.Name = "picPreview";
            this.picPreview.Size = new System.Drawing.Size(501, 609);
            this.picPreview.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picPreview.TabIndex = 1;
            this.picPreview.TabStop = false;
            // 
            // picCrop
            // 
            this.picCrop.Location = new System.Drawing.Point(544, 12);
            this.picCrop.Name = "picCrop";
            this.picCrop.Size = new System.Drawing.Size(501, 609);
            this.picCrop.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picCrop.TabIndex = 2;
            this.picCrop.TabStop = false;
            // 
            // optGrayscale
            // 
            this.optGrayscale.AutoSize = true;
            this.optGrayscale.Checked = true;
            this.optGrayscale.Location = new System.Drawing.Point(365, 649);
            this.optGrayscale.Name = "optGrayscale";
            this.optGrayscale.Size = new System.Drawing.Size(124, 17);
            this.optGrayscale.TabIndex = 3;
            this.optGrayscale.TabStop = true;
            this.optGrayscale.Text = "Convert to Grayscale";
            this.optGrayscale.UseVisualStyleBackColor = true;
            this.optGrayscale.CheckedChanged += new System.EventHandler(this.optGrayscale_CheckedChanged);
            // 
            // optAsIs
            // 
            this.optAsIs.AutoSize = true;
            this.optAsIs.Location = new System.Drawing.Point(558, 649);
            this.optAsIs.Name = "optAsIs";
            this.optAsIs.Size = new System.Drawing.Size(48, 17);
            this.optAsIs.TabIndex = 4;
            this.optAsIs.Text = "As Is";
            this.optAsIs.UseVisualStyleBackColor = true;
            this.optAsIs.CheckedChanged += new System.EventHandler(this.optAsIs_CheckedChanged);
            // 
            // chkCleanup
            // 
            this.chkCleanup.AutoSize = true;
            this.chkCleanup.Checked = true;
            this.chkCleanup.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkCleanup.Location = new System.Drawing.Point(662, 649);
            this.chkCleanup.Name = "chkCleanup";
            this.chkCleanup.Size = new System.Drawing.Size(156, 17);
            this.chkCleanup.TabIndex = 5;
            this.chkCleanup.Text = "Perform Cleanup after Scan";
            this.chkCleanup.UseVisualStyleBackColor = true;
            // 
            // chkCropContents
            // 
            this.chkCropContents.AutoSize = true;
            this.chkCropContents.Checked = true;
            this.chkCropContents.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkCropContents.Location = new System.Drawing.Point(662, 676);
            this.chkCropContents.Name = "chkCropContents";
            this.chkCropContents.Size = new System.Drawing.Size(104, 17);
            this.chkCropContents.TabIndex = 9;
            this.chkCropContents.Text = "Crop to contents";
            this.chkCropContents.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.optWeightHeavy);
            this.groupBox1.Controls.Add(this.optWeightNormal);
            this.groupBox1.Controls.Add(this.optWeightNone);
            this.groupBox1.Location = new System.Drawing.Point(352, 670);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(200, 100);
            this.groupBox1.TabIndex = 10;
            this.groupBox1.TabStop = false;
            // 
            // optWeightHeavy
            // 
            this.optWeightHeavy.AutoSize = true;
            this.optWeightHeavy.Location = new System.Drawing.Point(41, 66);
            this.optWeightHeavy.Name = "optWeightHeavy";
            this.optWeightHeavy.Size = new System.Drawing.Size(117, 17);
            this.optWeightHeavy.TabIndex = 11;
            this.optWeightHeavy.Text = "Text Weight Heavy";
            this.optWeightHeavy.UseVisualStyleBackColor = true;
            // 
            // optWeightNormal
            // 
            this.optWeightNormal.AutoSize = true;
            this.optWeightNormal.Location = new System.Drawing.Point(41, 43);
            this.optWeightNormal.Name = "optWeightNormal";
            this.optWeightNormal.Size = new System.Drawing.Size(119, 17);
            this.optWeightNormal.TabIndex = 10;
            this.optWeightNormal.Text = "Text Weight Normal";
            this.optWeightNormal.UseVisualStyleBackColor = true;
            // 
            // optWeightNone
            // 
            this.optWeightNone.AutoSize = true;
            this.optWeightNone.Checked = true;
            this.optWeightNone.Location = new System.Drawing.Point(41, 18);
            this.optWeightNone.Name = "optWeightNone";
            this.optWeightNone.Size = new System.Drawing.Size(112, 17);
            this.optWeightNone.TabIndex = 9;
            this.optWeightNone.TabStop = true;
            this.optWeightNone.Text = "Text Weight None";
            this.optWeightNone.UseVisualStyleBackColor = true;
            // 
            // lblWarning
            // 
            this.lblWarning.AutoSize = true;
            this.lblWarning.Font = new System.Drawing.Font("Microsoft Tai Le", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWarning.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblWarning.Location = new System.Drawing.Point(558, 730);
            this.lblWarning.Name = "lblWarning";
            this.lblWarning.Size = new System.Drawing.Size(0, 23);
            this.lblWarning.TabIndex = 11;
            // 
            // chkIdCard
            // 
            this.chkIdCard.AutoSize = true;
            this.chkIdCard.Location = new System.Drawing.Point(837, 650);
            this.chkIdCard.Name = "chkIdCard";
            this.chkIdCard.Size = new System.Drawing.Size(90, 17);
            this.chkIdCard.TabIndex = 12;
            this.chkIdCard.Text = "Id Card Mode";
            this.chkIdCard.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1062, 779);
            this.Controls.Add(this.chkIdCard);
            this.Controls.Add(this.lblWarning);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.chkCropContents);
            this.Controls.Add(this.chkCleanup);
            this.Controls.Add(this.optAsIs);
            this.Controls.Add(this.optGrayscale);
            this.Controls.Add(this.picCrop);
            this.Controls.Add(this.picPreview);
            this.Controls.Add(this.button1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.picPreview)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCrop)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.PictureBox picPreview;
        private System.Windows.Forms.PictureBox picCrop;
        private System.Windows.Forms.RadioButton optGrayscale;
        private System.Windows.Forms.RadioButton optAsIs;
        private System.Windows.Forms.CheckBox chkCleanup;
        private System.Windows.Forms.CheckBox chkCropContents;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton optWeightHeavy;
        private System.Windows.Forms.RadioButton optWeightNormal;
        private System.Windows.Forms.RadioButton optWeightNone;
        private System.Windows.Forms.Label lblWarning;
        private System.Windows.Forms.CheckBox chkIdCard;
    }
}

