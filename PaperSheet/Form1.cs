﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Emgu.CV;
using Emgu.CV.Structure;

using System.Runtime.InteropServices;
using Emgu.CV.CvEnum;
using System.Diagnostics;
using TigerIT.ImageProcessing.DocumentScanner;



namespace PaperSheet
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private Image<Bgr, Byte> inputImage ;

        

        private void button1_Click(object sender, EventArgs e)
        {

            //OpenFileDialog o = new OpenFileDialog();
            //if (o.ShowDialog() == DialogResult.OK){
            //    //ScannerOption opt = new ScannerOption();
                
            //    //opt.CropTextContets = false;
            //    //opt.DoCleanupAfterScan = true;
            //    //opt.ScanType = ScanType.ConvertToGrayscale; 
            //    //opt.TextWeight = GrayscaleTextWeight.NORMAL;
                

            //    //DocumentScanner ds = new DocumentScanner(opt);
            //    Bitmap bmp = (Bitmap)Bitmap.FromFile(o.FileName);
            //    bmp = TestBed.detectCard(bmp);
            //    //bmp = TestBed.detectPaperTest(bmp);
            //    //bmp = TestBed.detectPaperUsingChannelSeperation(bmp);
            //    picPreview.Image = bmp ;
            //    //Bitmap bmp2 = ds.Scan(bmp);
            //    //picPreview.Image = bmp2;
            //    bmp.Save("bmp.jpg");
            //    //detectText2(bmp2);
                
                

                
            //}
                
            //return;

            ScannerOption opt = new ScannerOption();
            
            if (optGrayscale.Checked){
                opt.ScanType = ScanType.ConvertToGrayscale;

                if (optWeightNone.Checked)
                    opt.TextWeight = GrayscaleTextWeight.NONE;
                if (optWeightNormal.Checked)
                    opt.TextWeight = GrayscaleTextWeight.NORMAL;
                if (optWeightHeavy.Checked)
                    opt.TextWeight = GrayscaleTextWeight.HEAVY;

                opt.IdCardMode = chkIdCard.Checked;
            }

            if (optAsIs.Checked)
                opt.ScanType = ScanType.AsIs;

            if (chkCleanup.Checked)
                opt.DoCleanupAfterScan = true;
            else
                opt.DoCleanupAfterScan = false;

            if (chkCropContents.Checked)
                opt.CropTextContets = true;
            else
                opt.CropTextContets = false;



            DocumentScanner ds = new DocumentScanner(opt);

            OpenFileDialog Openfile = new OpenFileDialog();
            if (Openfile.ShowDialog() == DialogResult.OK)
            {
                
                //Load the Image
                Bitmap bmp = (Bitmap)Image.FromFile(Openfile.FileName);

                picPreview.Image = bmp;

                Bitmap scan = ds.Scan(bmp);

                picCrop.Image = scan;

                SaveFileDialog sfd = new SaveFileDialog();
                sfd.Filter = "*.png | PNG" ;
                if (sfd.ShowDialog() == DialogResult.OK){
                    CompressionUtility.CompressAndSave(scan, sfd.FileName, 50);
                    //scan.Save(sfd.FileName);
                }
                bmp.Dispose();
                scan.Dispose();
                ds = null;
                System.GC.Collect();
            }
            
        }


        
       
        

        
        private void optGrayscale_CheckedChanged(object sender, EventArgs e)
        {
            if (optGrayscale.Checked){
                optAsIs.Checked = false;
                optWeightNormal.Enabled = true;
                optWeightNone.Enabled = true;
                optWeightHeavy.Enabled = true;
                optWeightNormal.Checked = true;
                lblWarning.Text = "" ;
            }else{
                   optAsIs.Checked = true;
                optWeightNormal.Enabled = false;
                optWeightNone.Enabled = false;
                optWeightHeavy.Enabled = false;
                optWeightNormal.Checked = false;
            }
        }

        private void optAsIs_CheckedChanged(object sender, EventArgs e)
        {
            if (optAsIs.Checked){
                optGrayscale.Checked = false;
                optWeightNormal.Enabled = false;
                optWeightNone.Enabled = false;
                optWeightHeavy.Enabled = false;
                lblWarning.Text = "Cleanup and cropping contents works best in grayscale mode";
            }
        }

        private void optWeightNone_CheckedChanged(object sender, EventArgs e)
        {
            if (optWeightNone.Checked){
                optWeightHeavy.Checked = false;
                optWeightNormal.Checked = false;
            }
        }

        private void optWeightNormal_CheckedChanged(object sender, EventArgs e)
        {
            if (optWeightNormal.Checked){
                optWeightHeavy.Checked = false;
                optWeightNone.Checked = false;
            }
        }

        private void optWeightHeavy_CheckedChanged(object sender, EventArgs e)
        {
            if (optWeightHeavy.Checked){
                optWeightNormal.Checked = false;
                optWeightNone.Checked = false;
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
